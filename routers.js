const express = require('express')
const { ObjectId } = require('mongodb')
const router = express()
// const connection = require('./connection')
const mongoose = require('./mongoose')
const User = require('./User')

router.get('/', (req, res) => {
  res.send('Hello World!')
})

router.get('/users', async (req, res) => {
  try {
    const users = await User.find();
    res.send({ data: users });
  } catch (err) {
    res.send({ message: err.message || "Internal Server Error" });
  }
})

router.get('/users/:id', async (req, res) => {
  try {
    const { id } = req.params
    const user = await User.findOne({ _id: id });
    if (user) {
      res.send({ data: user });
    } else {
      res.send({ message: 'User tidak ditemukan' });
    }
  } catch (err) {
    res.send({ message: err.message || "Internal Server Error" });
  }
})

router.post('/users', async (req, res) => {
  try {
    const { name, age, status } = req.body
    const user = await User.create({
      name, age, status
    });
    res.send({ data: user });
  } catch (err) {
    res.send({ message: err.message || "Internal Server Error" });
  }
})

router.put('/users/:id', async (req, res) => {
  try {
    const { id } = req.params
    const { name, age, status } = req.body
    const user = await User.updateOne({ _id: id }, {
      name, age, status
    }, { runValidators: true });
    if (user) {
      res.send({ data: user });
    } else {
      res.send({ message: "User tidak ditemukan" });
    }
  } catch (err) {
    res.send({ message: err.message || "Internal Server Error" });
  }
})

router.delete('/users/:id', async (req, res) => {
  try {
    const { id } = req.params
    const user = await User.deleteOne({ _id: id });
    if (user) {
      res.send({ data: user });
    } else {
      res.send({ message: "User tidak ditemukan" });
    }
  } catch (err) {
    res.send({ message: err.message || "Internal Server Error" });
  }
})

// router.get('/users', async (req, res) => {
//   try {
//     if (connection) {
//       const db = connection.db('db_latihan')
//       const users = await db.collection('users').find().toArray()
//       res.send({ data: users })
//     } else {
//       res.send({ message: "Koneksi Database Gagal" })
//     }
//   } catch (err) {
//     res.send({ message: err.message || "Internal Server Error" });
//   }
// })

// router.post('/users', async (req, res) => {
//   try {
//     if (connection) {
//       const { name, age, status } = req.body
//       const db = connection.db('db_latihan')
//       const users = await db.collection('users').insertOne({
//         name,
//         age,
//         status
//       })
//       if (users.insertedCount() === 1) {
//         res.send({ message: "Berhasil ditambahkan" })
//       } else {
//         res.send({ message: "Gagal menambahkan user" })
//       }
//     } else {
//       res.send({ message: "Koneksi Database Gagal" })
//     }
//   } catch (err) {
//     res.send({ message: err.message || "Internal Server Error" });
//   }
// })

// router.put('/users/:id', async (req, res) => {
//   try {
//     if (connection) {
//       const { id } = req.params
//       const { name, age, status } = req.body
//       const db = connection.db('db_latihan')
//       const users = await db.collection('users').updateOne({ _id: ObjectId(id) }, {
//         $set: {
//           name,
//           age,
//           status
//         }
//       })
//       if (users.modifiedCount() === 1) {
//         res.send({ message: "Berhasil diubah" })
//       } else {
//         res.send({ message: "Gagal mengubah user" })
//       }
//     } else {
//       res.send({ message: "Koneksi Database Gagal" })
//     }
//   } catch (err) {
//     res.send({ message: err.message || "Internal Server Error" });
//   }
// })

// router.delete('/users/:id', async (req, res) => {
//   try {
//     if (connection) {
//       const { id } = req.params
//       const db = connection.db('db_latihan')
//       const users = await db.collection('users').deleteOne({ _id: ObjectId(id) })
//       if (users.deletedCount() === 1) {
//         res.send({ message: "Berhasil dihapus" })
//       } else {
//         res.send({ message: "Gagal menghapus user" })
//       }
//     } else {
//       res.send({ message: "Koneksi Database Gagal" })
//     }
//   } catch (err) {
//     res.send({ message: err.message || "Internal Server Error" });
//   }
// })

module.exports = router