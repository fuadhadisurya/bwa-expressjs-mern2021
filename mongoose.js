const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/db_latihan', { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;
db.on('error', console.error.bind(console, "connection error:"));
db.once('open', () => {
    console.log("Server Database Connect");

    // Get Data
    // const user = await User.findOne();
    // console.log(user);

    // Create
    // const newUser = await User.create({
    //     name: "Fuad Hadisurya",
    //     age: 21,
    //     status: 'active'
    // })
    // console.log(newUser);

    // const newUser = new User();
    // newUser.name = "Tio";
    // newUser.age = 23;
    // newUser.status = 'non active';
    // const insert = await newUser.save()
    // console.log(insert);

    // Update
    // const updateUser = await User.updateOne({_id: '6331a2da285a1ee69feb7b38'}, {name: "Tio Saputra"})
    // console.log(update);

    // const updateUser = await User.findById('6331a2da285a1ee69feb7b38');
    // updateUser.name = "Tio";
    // const update = await updateUser.save();
    // console.log(update);

    // Delete
    // const deleteUser = await User.deleteOne({_id: "6331a2da285a1ee69feb7b38"})
    // console.log(deleteUser);

    // const userCreate = await User.create({
    //     name: "Tio",
    //     age:24
    // });
    // console.log(userCreate);
});